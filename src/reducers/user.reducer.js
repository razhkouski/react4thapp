import { EDIT_USER } from "../constants/user.constants";
import { user as defaultState } from "../defaultStates/user";

export const userReducer = (state = defaultState, action) => {
    switch (action.type) {
        case EDIT_USER:
            let newState = {
                name: action.data.name,
                surname: action.data.surname,
                card: action.data.card
            };

            return newState;
        default:
            return state;
    }
};