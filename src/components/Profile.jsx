import React, { useState, useContext } from 'react';
import Context from '../utils/context';

const Profile = () => {
    const context = useContext(Context);
    const [name, setName] = useState(context.userState.name);
    const [surname, setSurname] = useState(context.userState.surname);
    const [card, setCard] = useState(context.userState.card);

    const handleEdit = () => {
        let user = {
            name: name ? name : context.userState.name,
            surname: surname ? surname : context.userState.surname,
            card: card ? card : context.userState.card
        };

        context.editUser(user);

        setName('');
        setSurname('');
        setCard('');
    }

    return (
        <>
            <h1>User information</h1>
            <div key={context.userState.id}>
                <p>{context.userState.name} {context.userState.surname}</p>
                <p>Card number: {context.userState.card}</p>
            </div>
            Name: <input value={name} onChange={(e) => setName(e.target.value)}></input>
            Surname: <input value={surname} onChange={(e) => setSurname(e.target.value)}></input>
            Card number: <input value={card} onChange={(e) => setCard(e.target.value)}></input>
            <button onClick={() => handleEdit(context.userState)}>Edit</button>
        </>
    )
};

export default Profile;